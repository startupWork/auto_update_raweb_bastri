import requests
import json
import csv
import re
from html.parser import HTMLParser
import configparser
from elasticsearch import Elasticsearch
from bs4 import BeautifulSoup
import datetime
import teams_trigrams_significant

def get_csv_from_bastri() :
    """
        Get csv file from bastri, Create or Update team
    """
    r = requests.get("https://bastri.inria.fr/export/structurerecherche/exportCSVFiches/0")
    csv_row = r.text.split('\n')
    return csv_row

def manage_new_team(csv_row) :
    i = 0
    for row in csv_row :
        if i > 0 :
            info_team = row.split(";")
            for j in range(0, len(info_team)) :
                info_team[j] = info_team[j].replace('"', '')
            if len(info_team) > 18 :
                acronym = info_team[1]
                team = get_team(acronym)
                if team["hits"]["total"] == 0 :
                    # CREATE
                    create_team(info_team)
                else :
                    # UPDATE
                    update_team(team["hits"]["hits"][0], info_team)
        i+=1

def update_history_bastri(csv_row) :
    i = 0
    for row in csv_row :
        if i > 0 :
            info_team = row.split(";")
            for j in range(0, len(info_team)) :
                info_team[j] = info_team[j].replace('"', '')
            if len(info_team) > 18 :
                bastri_history = create_history_bastri(info_team[0])
                create_team_history_for_elastic(info_team[0], bastri_history)
        i+=1

def get_team(name) :
    """
        Get team in elasticsearch
    """
    query = {
    "query" : {"match" : {
        "acronym" : name
    }}
    }
    return es.search(index = index_team, doc_type = doc_type_team, body=query)

def update_team(team_source, info_team) :
    """
        Update info in team if info change in bastri
    """
    update_info = {}
    modif = False
    if info_team[1] != team_source["_source"]["acronym"] :
        update_info["acronym"] = info_team[1]
        modif = True
    if info_team[18].split("/")[2] < team_source["_source"]["date_start"].split("/")[2] :
        update_info["date_start"] = info_team[18]
        modif = True
    if info_team[19].split("/")[2] > team_source["_source"]["date_end"].split("/")[2] :
        update_info["date_end"] = info_team[19]
        modif = True
    if info_team[6] != team_source["_source"]["cri_bastri"] :
        update_info["cri_bastri"] = info_team[6]
        modif = True
    if info_team[7] != team_source["_source"]["site"] :
        update_info["site"] = info_team[7]
        modif = True
    if info_team[4] + " " + info_team[5] != team_source["_source"]["responsable"] :
        update_info["responsable"] = info_team[4] + " " + info_team[5]
        modif = True
    now = datetime.datetime.now()
    if int(team_source["_source"]["date_end"].split("/")[2]) < now.year :
        update_info["status"] = "OLD"
        modif = True
    if modif :
        es.update(index=index_team, doc_type=doc_type_team, id=team_source["_id"], body={"doc" : update_info})

def create_team(info_team) :
    """
        Create new team for Elasticsearch (New team in bastri)
    """
    new_info = {}
    new_info["acronym"] = info_team[1]
    new_info["date_start"] = info_team[18]
    new_info["date_end"] = info_team[19]
    new_info["cri_bastri"] = info_team[6]
    new_info["site"] = info_team[7]
    new_info["responsable"] = info_team[4] + " " + info_team[5]
    team_pubs = get_pub_of_team(new_info["acronym"])
    i = 0
    all_pubs = []
    if team_pubs["hits"]["total"] != 0 :
        for pub in team_pubs["hits"]["hits"] :
            if i == 0 :
                for aff in pub["_source"]["affiliations"]:
                    if aff["acronym"] == new_info["acronym"] :
                        new_info["id"] = aff["id"]
                        new_info["type"] = aff["type"]
                        new_info["address"] = aff["address"]
                        new_info["status"] = aff["status"]
                        new_info["country_key"] = aff["country_key"]
                        new_info["name"] = aff["name"]
                        new_info["url"] = aff["url"]
                i+=1
            del pub["_source"]["affiliations"]
            del pub["_source"]["projects"]
            all_pubs.append(pub["_source"])
        new_info["pubs"] = all_pubs
        new_info["main_topics"] = teams_trigrams_significant.teams_new_topics(all_pubs)
        es.index(index=index_team, doc_type=doc_type_team, id=new_info["id"], body=new_info)
    else :
        now = datetime.datetime.now()
        if int(info_team[19].split("/")[2]) < now.year :
            new_info["status"] = "OLD"
        else :
            new_info["status"] = "WAIT"
        es.index(index=index_team, doc_type=doc_type_team, id=info_team[0], body=new_info)

def create_history_bastri(siid) :
    """
        Create history of the team from bastri in a dictionary
    """
    r = requests.get('https://bastri.inria.fr/FichesEquipes/structurerecherche/show/'+siid)
    parsed_html = BeautifulSoup(r.text, 'lxml')
    text = parsed_html.find("p", {"class" : "genealogie"}).text
    hist = str(re.findall("[^\n\t' '()\xa0→]\w*", text))
    text = hist.replace('[', '')
    text = text.replace(']', '')
    text = text.replace("'", '')
    text = text.replace("'", '')
    all_team = text.split(',')
    allteam_strip = []
    if len(all_team) > 1 :
        i = 0
        j = 1
        for val in all_team :
            if i == 0 :
                name = val.strip()
            elif name == val.strip() :
                j = j + 1
            i = i +1
        if j != len(all_team) :
            allteam_strip = []
            i = 0
            for valnotstrip in all_team :
                containvalue = valnotstrip.strip()
                if "-" in containvalue :
                    containvalue = all_team[i - 1].strip() + containvalue
                    del allteam_strip[len(allteam_strip)- 1]
                if containvalue not in allteam_strip or containvalue == '' :
                    allteam_strip.append(containvalue)
                i = i + 1
    else :
        allteam_strip = []
    return allteam_strip

def create_team_history_for_elastic(key, value) :
    """
        Create team history to index in Elasticsearch
    """
    r = requests.get('https://bastri.inria.fr/FichesEquipes/structurerecherche/show/'+key)
    parsed_html = BeautifulSoup(r.text, 'lxml')
    text = parsed_html.findAll("h1")[1].text
    text = re.findall("\w*", text)[0]
    team = get_team(text)
    if len(team["hits"]["hits"]) != 0 :
        team_to_update = team["hits"]["hits"][0]
        team = team["hits"]["hits"][0]["_source"]
        if '' in value :
            if text == team["acronym"] :
                history_bastri = []
                i = len(value) - 1
                for val in reversed(value) :
                        if val != '' :
                            team_2 = get_team(val)
                            if len(team_2["hits"]["hits"]) != 0 :
                                team_2 = team_2["hits"]["hits"][0]["_source"]
                                if val == team_2["acronym"] :
                                    if i == len(value) - 1 :
                                        if "id" in team_2 :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "Y", "status" : team_2["status"]})
                                        else :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "Y", "status" : team_2["status"]})
                                    elif value[i + 1] == '' :
                                        if "id" in team_2 :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "Y", "status" : team_2["status"]})
                                        else :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "Y", "status" : team_2["status"]})
                                    else :
                                        if "id" in team_2 :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "N", "status" : team_2["status"]})
                                        else :
                                            history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "N", "status" : team_2["status"]})
                            else :
                                if i == len(value) - 1 :
                                    history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
                                elif value[i + 1] == '' :
                                    history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
                                else :
                                    history_bastri.append({"name" : val, "id_struct" : "", "split" : "N", "status" : "WAIT"})
                        i = i - 1
                history_bastri.reverse()
                team["history_bastri"] = history_bastri
                es.update(index=index_team, doc_type=doc_type_team, id=team_to_update["_id"], body={"doc" : team})
            else :
                print("Not in found " + value[0])
        else :
            if text == team["acronym"] :
                history_bastri = []
                i = len(value) - 1
                for val in value :
                        team_2 = get_team(val)
                        if len(team_2["hits"]["hits"]) != 0 :
                            team_2 = team_2["hits"]["hits"][0]["_source"]
                            if val == team_2["acronym"] :
                                if "id" in team_2 :
                                    history_bastri.append({"name" : team_2["acronym"], "id_struct" : team_2["id"], "split" : "N", "status" : team_2["status"]})
                                else :
                                    history_bastri.append({"name" : team_2["acronym"], "id_struct" : "", "split" : "N", "status" : team_2["status"]})
                        else :
                            if i == len(value) - 1 :
                                history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
                            elif value[i + 1] == '' :
                                history_bastri.append({"name" : val, "id_struct" : "", "split" : "Y", "status" : "WAIT"})
                            else :
                                history_bastri.append({"name" : val, "id_struct" : "", "split" : "N", "status" : "WAIT"})
                team["history_bastri"] = history_bastri
                es.update(index=index_team, doc_type=doc_type_team, id=team_to_update["_id"], body={ "doc" : team})
    else :
        print("Not in Elastic " + text)

def get_pub_of_team(acronym) :
    """
        Get publications for team acronym
    """
    get_pubs = {
    "size" : 10000,
                "query" : {
                    "bool": {
                        "must": [
                           {"nested": {
                           "path": "affiliations",
                           "query": { "match": {
                            "affiliations.acronym" : acronym
                        }}}},
                        {"nested": {
                           "path": "affiliations.relations.indirect",
                           "query": {
                               "match": {
                                  "affiliations.relations.indirect.acronym": "Inria"
                               }                                   }
                    }}
                ]
            }
        }
    }
    return es.search(index=index_pub, doc_type=doc_type_pub, body=get_pubs)


def update_wait_team() :
    """
        Update Team where no publications was found in last update
    """
    query_wait = {"size" : 100, "query" : {"match" : {"status" : "WAIT"}} }
    wait_team = es.search(index=index_team, doc_type=doc_type_team, body=query_wait)
    for team in wait_team["hits"]["hits"] :
        team_pubs = get_pub_of_team(team["_source"]["acronym"])
        i = 0
        all_pubs = []
        if team_pubs["hits"]["total"] != 0 :
            for pub in team_pubs["hits"]["hits"] :
                if i == 0 :
                    for aff in pub["_source"]["affiliations"]:
                        if aff["acronym"] == team["_source"]["acronym"] :
                            team["_source"]["id"] = aff["id"]
                            team["_source"]["type"] = aff["type"]
                            team["_source"]["address"] = aff["address"]
                            team["_source"]["status"] = aff["status"]
                            team["_source"]["country_key"] = aff["country_key"]
                            team["_source"]["name"] = aff["name"]
                            team["_source"]["url"] = aff["url"]

                    i+=1
                del pub["_source"]["affiliations"]
                del pub["_source"]["projects"]
                all_pubs.append(pub["_source"])
            now = datetime.datetime.now()
            old_date = team["_source"]["date_end"].split("/")
            if len(old_date) > 1 :
                if int(old_date[2]) < now.year :
                    team["_source"]["status"] = "OLD"
                else :
                    team["_source"]["status"] = "VALID"
            else :
                if int(team["_source"]["date_end"]) < now.year :
                    team["_source"]["status"] = "OLD"
                else :
                    team["_source"]["status"] = "VALID"
            team["_source"]["pubs"] = all_pubs
            team["_source"]["main_topics"] = teams_trigrams_significant.teams_new_topics(all_pubs)
            es.update(index=index_team, doc_type=doc_type_team, id=team["_source"]["id"], body={"doc" : team["_source"]})
            es.delete(index=index_team, doc_type=doc_type_team, id=team["_id"])

if __name__ == '__main__':
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    es = Elasticsearch(config.get("elasticsearch", "ip"))
    index_team = config.get("elasticsearch", "index_team")
    doc_type_team = config.get("elasticsearch", "doc_type_team")
    index_pub = config.get("elasticsearch", "index_pub")
    doc_type_pub = config.get("elasticsearch", "doc_type_pub")
    csv_file = get_csv_from_bastri()
    manage_new_team(csv_file)
    update_wait_team()
    update_history_bastri(csv_file)
