import configparser
from bs4 import BeautifulSoup
import requests
import re
import sys
from elasticsearch import Elasticsearch
import teams_trigrams_significant

def replace_special_caract(text) :
    text = text.replace("é", "e")
    text = text.replace("è", "e")
    text = text.replace("ê", "e")
    text = text.replace("ë", "e")
    text = text.replace("à", "a")
    text = text.replace("ï", "i")
    text = text.replace("î", "i")
    text = text.replace("ô", "o")
    text = text.replace("ç", "c")
    text = text.replace("ü", "u")
    text = text.replace("ù", "u")
    text = text.replace("-", " ")
    return text

def get_team_composition(acronym, year):
    team_structure = dict()
    team_members = []
    team_structure["acronym"] = acronym.lower()
    team_structure["year"] = year
    link_to_parse = "https://raweb.inria.fr/rapportsactivite/RA" +str(year)+"/" +str(acronym.strip().lower())+"/" +str(acronym.strip().lower()) + ".xml"
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    team = soup.find("team")
    if bool(team) :
        results = team.findAll("person")
        if bool(results):
            for result in results:
                member = dict()

                if result.has_attr('key'):
                    key = result['key'].strip()
                    if key == "PASUSERID":
                        member["key"] = ""
                    else:
                        member["key"] = key
                else:
                    member["key"] = ""

                category = ""
                research_centre_text = ""
                affiliation_text = ""
                moreinfos = ""
                for child in result.findChildren():
                    if child.name == 'categorypro':
                        category = child.text.strip().lower()
                    if child.name == 'affiliation':
                        affiliation_text = child.text.strip()
                    if child.name == 'research-centre':
                        research_centre_text = child.text.strip()
                    if child.name == 'moreinfo':
                        moreinfos = child.text.strip()
                member["year"] = year
                member["category"] = category
                member["firstname"] = result.firstname.text.strip()
                member["lastname"] = result.lastname.text.strip()
                member["moreinfos"] = moreinfos + " # " + affiliation_text + " # " + research_centre_text
                team_members.append(member)
    team_structure["members"] = team_members
    return team_structure

def get_team_activities_raweb(year):
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    link = star_url + year + "/" + end_url
    response = requests.get(link)
    soup = BeautifulSoup(response.content, "html.parser")
    result = soup.find("div", {"id": "listeAlpha"})
    list_teams_same_year = []
    if result:
        children = result.findChildren()
        for child in children:
            team = {}
            if child.name == "a" and child.has_attr("href"):
                team["acronym"] = child.text.strip().lower()
                team["url"] = star_url + year + "/" + child['href']
                team["year"] = int(year)
                list_teams_same_year.append(team)
    return list_teams_same_year

#Only for individuals
def elasticsearch_update(members) :
    for member in members["members"] :
        formatName = replace_special_caract(member["firstname"].lower()+" "+member["lastname"].lower())
        get_author = {
            "query" : {"match" : {"formatName" : formatName} }
        }
        result_indiv = es.search(index=index_author, doc_type=doc_type_author, body= get_author)
        to_update = dict()
        if result_indiv["hits"]["total"] == 1 :
            source = result_indiv["hits"]["hits"][0]["_source"]
            if source["category"] != member["category"]:
                to_update["category"] = member["category"]
            to_update["last_appear"] = int(members["year"])
            i = 0
            pass_history = False
            for his in source["history"] :
                if members["acronym"].upper() == his["acronym"] :
                    pass_history = True
                    source["history"][i]["last_appear"] = int(members["year"])
                i= i + 1
            if not pass_history :
                get_team = {
                    "query" : {"match" : {"acronym" : members["acronym"].upper()} }
                }
                result_team = es.search(index=index_team, doc_type=doc_type_team, body= get_team)
                if result_team["hits"]["total"] >= 1 :
                    new_id = ""
                    if result_team["hits"]["total"] > 1 :
                        for r in result_team["hits"]["hits"] :
                            if "struct" in r["_id"] :
                                new_id = r["_id"]
                    else :
                        new_id = result_team["hits"]["hits"][0]["_id"]
                    source["history"].append({  "moreinfos": member["moreinfos"],
                                            "last_appear": int(members["year"]),
                                            "structure": new_id,
                                            "category": member["category"],
                                            "first_appear": int(members["year"]),
                                            "acronym": members["acronym"].upper() })
                else :
                    print(members["acronym"])
            to_update["history"] = source["history"]
            es.update(index=index_author, doc_type=doc_type_author, id=result_indiv["hits"]["hits"][0]["_id"] ,body={"doc" : to_update}, timeout="60s")
        else :
            get_team = {
                "query" : {"match" : {"acronym" : members["acronym"].upper()} }
            }
            result_team = es.search(index=index_team, doc_type=doc_type_team, body= get_team)
            new_id = ""
            if result_team["hits"]["total"] > 1 :
                for r in result_team["hits"]["hits"] :
                    if "struct" in r["_id"] :
                        new_id = r["_id"]
            else :
                new_id = result_team["hits"]["hits"][0]["_id"]
            new_researcher = {
                  "Allname": [
                     member["firstname"]+" "+member["lastname"]
                  ],
                  "forename": member["firstname"],
                  "category": member["category"],
                  "history": [
                     {  "moreinfos": member["moreinfos"],
                        "last_appear": int(members["year"]),
                        "structure": new_id,
                        "category": member["category"],
                        "first_appear": int(members["year"]),
                        "acronym": members["acronym"] }
                  ],
                  "pubs": [],
                  "first_appear": int(members["year"]),
                  "moreinfos": member["moreinfos"],
                  "last_appear": int(members["year"]),
                  "formatName": formatName,
                  "structure": new_id,
                  "surname": member["lastname"],
                  "acronym": members["acronym"]
             }
            get_pub_author = {
                "query" : {"nested" : {"path" : "authors", "query" : {"bool" : {"must" : [{
                "match" : { "authors.surname" : member["lastname"]}}, {"match" : {"authors.forname" : member["firstname"]}}, {"match" : {"authors.mail_domain" : "inria.fr"}}]}}}}
            }
            all_pub = es.search(index=index_pub, doc_type=doc_type_pub, body=get_pub_author)
            for pub in all_pub["hits"]["hits"] :
                del pub["_source"]["affiliations"]
                del pub["_source"]["projects"]
                new_researcher["pubs"].append(pub["_source"])
            new_researcher["main_topics"] = teams_trigrams_significant.teams_new_topics(new_researcher["pubs"])
            es.index(index=index_author, doc_type=doc_type_author, body=new_researcher, timeout="60s")

if __name__ == '__main__':
    config = configparser.RawConfigParser()
    config.read("ConfigFile.properties")
    year = config.get("elasticsearch", "year")
    es = Elasticsearch(config.get("elasticsearch", "ip"))
    index_team = config.get("elasticsearch", "index_team")
    doc_type_team = config.get("elasticsearch", "doc_type_team")
    index_pub = config.get("elasticsearch", "index_pub")
    doc_type_pub = config.get("elasticsearch", "doc_type_pub")
    index_author = config.get("elasticsearch", "index_author")
    doc_type_author = config.get("elasticsearch", "doc_type_author")
    all_teams = get_team_activities_raweb(year)
    for all_team in all_teams :
        all_members = get_team_composition(all_team["acronym"], year)
        elasticsearch_update(all_members)
